# py3-pygments

* [pkgs.alpinelinux.org](https://pkgs.alpinelinux.org/packages?name=py3-pygments)
* [pypi.org](https://pypi.org/project/Pygments/)
* [pygments.org](http://pygments.org/)/docs/[lexers](http://pygments.org/docs/lexers/)

## License
* BSD 2-clauses
* [LICENSE](https://bitbucket.org/birkenfeld/pygments-main/src)

## Python3 packages and Alpine
* [alpinelinux-packages-demo/python3](https://gitlab.com/alpinelinux-packages-demo/python3)

## How to chose a language for a configuation file with semicolon separated comments
* [language semicolon comment](https://www.google.com/search?q=language+semicolon+comment)
  * [*Comparison of programming languages (syntax)*](https://en.wikipedia.org/wiki/Comparison_of_programming_languages_(syntax)#Comments)
  * [*Comments*](http://www.gavilan.edu/csis/languages/comments.html)